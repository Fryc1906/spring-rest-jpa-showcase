package com.fryc.meal;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fryc.ingredient.Ingredient;

@Entity
@Table(name="meal")
public class Meal {
	
	// FIELDS
	@Id
	@Column(name="meal_id")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@ManyToMany
	@JoinTable(
		name="meal_ingredient",
		joinColumns = @JoinColumn(name="meal_id"),
		inverseJoinColumns = @JoinColumn(name="ingredient_id")
		)
	private List<Ingredient> ingredients;
	
	// CONSTRUCTORS
	public Meal(){
		
	}
	
	// GETTERS AND SETTERS
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	
	// METHODS

}
