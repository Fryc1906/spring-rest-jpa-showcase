package com.fryc.ingredient;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import com.fryc.category.Category;
import com.fryc.meal.Meal;

@Entity
@Table(name="ingredient")
public class Ingredient {

	// FIELDS
	@Id
	@Column(name="ingredient_id")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="kcal")
	private short kcal;
	
	@ManyToMany
	@JoinTable(
		name="ingredient_category",
		joinColumns = @JoinColumn(name="ingredient_id"),
		inverseJoinColumns = @JoinColumn(name="category_id")
		)
	private List<Category> categories;
	
	@ManyToMany
	@JoinTable(
		name="meal_ingredient",
		joinColumns = @JoinColumn(name="ingredient_id"),
		inverseJoinColumns = @JoinColumn(name="meal_id")
		)
	private List<Meal> meals;
		
	// CONSTRUCTORS
	public Ingredient(){
		
	}
	
	// GETTERS AND SETTERS
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public short getKcal() {
		return kcal;
	}

	public void setKcal(short kcal) {
		this.kcal = kcal;
	}
		
	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public List<Meal> getMeals() {
		return meals;
	}

	public void setMeals(List<Meal> meals) {
		this.meals = meals;
	}

	// METHODS
	public void addMeal(Meal meal){
		if(meal == null){
			meals = new ArrayList<>();
		}
		
		meals.add(meal);
	}
	
	public void addCategory(Category category){
		if(category == null){
			categories = new ArrayList<>();
		}
		
		categories.add(category);
	}
	
}
